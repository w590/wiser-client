FROM node:16-alpine AS build

ARG API_URL
ENV VUE_APP_API_URL=${API_URL}

RUN mkdir "/src"

COPY . /src

WORKDIR /src

RUN yarn

RUN yarn build

FROM nginx AS runtime

COPY --from=build /src/dist /app

COPY nginx.conf /etc/nginx/nginx.conf
