import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Home from '../views/Home.vue'
import store from '../store/index';
import Profile from "../views/Profile";
import Register from "../views/Register";
import Books from "../views/Books";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        meta: {
            auth: true,
        }
    },
    {
        path: '/profile',
        name: 'Profile',
        component: Profile,
        meta: {
            auth: true,
        }
    },
    {
        path: '/books',
        name: 'Books',
        component: Books,
        meta: {
            auth: true,
        }
    },
    {
        path: '/login',
        name: 'Login',
        component: Login,
    },
    {
        path: '/register',
        name: 'Register',
        component: Register,
    }

]

const router = new VueRouter({
    mode: 'history',
    routes
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(x => x.meta.auth)) {
        if (store.state.auth.authorized || localStorage.getItem('token')) {
            next();
        } else {
            next('/login');
        }
    } else {
        next();
    }
});

export default router
