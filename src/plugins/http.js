import store from '../store/index';

async function refresh(url) {
    const response = await fetch(url, {
        method: 'GET',
        credentials: 'include',
    });
    if (response.status === 200) {
        const token = (await response.json()).token;
        localStorage.setItem('token', token);
        return true;
    }
    return false;
}

function fetcher() {
    const baseUrl = process.env.VUE_APP_API_URL;
    return async function (url, options) {
        const path = new URL(url, baseUrl).href;
        let token = localStorage.getItem('token');
        options.headers ||= {};
        options.headers['Authorization'] = `Bearer ${token}`;
        if (options.method && (options.method === 'POST' || options.method === 'PUT')) {
            options.headers['Accept'] = 'application/json';
            options.headers['Content-Type'] = 'application/json';
        }
        let response = await fetch(path, options);
        if (!options.ignoreAuthorization) {
            if (response.status === 401) {
                await refresh(new URL('user/token', baseUrl).href);
                token = localStorage.getItem('token');
                options.headers['Authorization'] = `Bearer ${token}`;
                response = await fetch(path, options);
            }
            if (response.status === 401) {
                await store.dispatch('auth/logout');
                return response;
            }
        }
        return response;
    }
}

export default fetcher();

