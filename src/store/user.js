import http from '../plugins/http'

export default {
    namespaced: true,
    state: {
        name: null,
        email: null,
    },
    mutations: {
        setName(state, name) {
            state.name = name;
        },

        setEmail(state, email) {
            state.email = email;
        }
    },
    actions: {
        async fetch(context) {
            const response = await http('/user', {
                method: 'GET'
            })
            if (response) {
                const user = await response.json();
                context.commit('setName', user.name);
                context.commit('setEmail', user.email);
            }
        }
    }
}