import http from '../plugins/http'

export default {
    namespaced: true,
    state: {
        authorized: false,
    },
    mutations: {
        login(state) {
            state.authorized = true;
        },

        logout(state) {
            state.authorized = false;
        }
    },
    actions: {
        async logout(context) {
            context.commit('logout');
            await this.$app.$http('/user/token', {
                method: 'DELETE',
                ignoreAuthorization: true,
                credentials: 'include'
            });
            localStorage.removeItem('token');
            await this.$app.$router.push('/login');
        },

        async login(context, user) {
            const response = await http('user/login', {
                method: 'POST',
                ignoreAuthorization: true,
                body: JSON.stringify(user),
                credentials: 'include'
            });
            if (response.status !== 200)
                return false;

            context.commit('login');
            const token = (await response.json()).token;
            localStorage.setItem('token', token);
            return true;
        },

        async register(context, user) {
            const response = await http('user/register', {
                method: 'POST',
                ignoreAuthorization: true,
                body: JSON.stringify(user),
                credentials: 'include'
            });
            if (response.status !== 200)
                return false;

            context.commit('login');
            const token = (await response.json()).token;
            debugger
            localStorage.setItem('token', token);
            return true;
        }

    }
};