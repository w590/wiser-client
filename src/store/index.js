import Vue from 'vue'
import auth from './auth'
import user from './user'
import books from './books'
import book from './book'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        snackBar: null,
        showSnackbar: false
    },
    mutations: {
        snackBar(state, value) {
            state.showSnackbar = value;
        },
        setSnackbar(state, snackBar) {
            state.snackBar = snackBar;
        }
    },
    actions: {
        showSnackbar(context, {text, timeout = 2000, color}) {
            if (context.state.snackBar) {
                context.state.snackBar.text = text;
                context.state.snackBar.color = color;
                context.commit('snackBar', true);
                setTimeout(() => {
                    context.commit('snackBar', false);
                }, timeout);
            }
        }
    },
    modules: {
        auth,
        user,
        books,
        book,
    }
})
