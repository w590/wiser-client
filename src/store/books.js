import http from '../plugins/http'

export default {
    namespaced: true,
    state: {
        books: [],
    },

    mutations: {
        setBooks(state, books) {
            state.books = books;
        },

        addBook(state, book) {
            state.books.push(book);
        },

        deleteBook(state, id) {
            state.books.splice(state.books.findIndex(x => x.id === id), 1);
        }
    },

    actions: {
        async fetch(context) {
            const response = await http('/books', {
                method: 'GET'
            });
            if (!response)
                return;

            const books = (await response.json()).data;
            context.commit('setBooks', books);
        },

        async add(context, book) {
            if (context.state.books.length === 0)
                await context.dispatch('fetch');

            const response = await http('/book', {
                method: 'POST',
                body: JSON.stringify(book),
            });

            if (!response)
                return;

            const newBook = (await response.json()).data;
            context.commit('addBook', newBook);
            if (!context.rootState.book.current || context.rootState.book.current.isCompleted)
                context.commit('book/setCurrentBook', newBook, {
                    root: true
                });
        },

        async delete(context, book) {
            if (context.state.books.length === 0)
                await context.dispatch('fetch');

            const response = await http(`/book?id=${book.id}`, {
                method: 'DELETE',
            });

            if (!response)
                return;

            context.commit('deleteBook', book.id);
        }
    }
}