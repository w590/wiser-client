import http from '../plugins/http'

export default {
    namespaced: true,
    state: {
        current: null,
    },
    mutations: {
        setCurrentBook(state, book) {
            state.current = book;
        },

        addPages(state, pages) {
            state.current.currentPage += pages;
            if (state.current.currentPage > state.current.pagesCount)
                state.current.currentPage = state.current.pagesCount;
        }
    },

    actions: {
        async fetch(context) {
            const response = await http('/book', {
                method: 'GET'
            });

            if (!response)
                return;

            const book = (await response.json()).data;
            context.commit('setCurrentBook', book);
        },

        async addProgress(context, pages) {
            const response = await http('/book', {
                method: 'PUT',
                body: JSON.stringify({
                    pagesCount: pages
                }),
            });

            if (!response)
                return;

            const book = (await response.json()).data;
            context.commit('setCurrentBook', book);
            if (book.isCompleted)
                await context.dispatch('fetch');
        }
    }
}